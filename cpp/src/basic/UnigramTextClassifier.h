// http://entish.org/realquickcpp/classesetal.html
#ifndef UnigramTextClassifier_H
#define UnigramTextClassifier_H
#include <map>
#include <iostream>
#include <fstream>

namespace TextClassifier 
{
  typedef map<unsigned char,unsigned long> frequency_map;
  class UnigramTextClassifier 
  {
  public:
    UnigramTextClassifier();
    UnigramTextClassifier(const string classification);

    frequency_map freqs() { return my_freqs; }
    unsigned long corpus_total() { return my_corpus_total; }
    unsigned long total() { return my_total; }
    string classification() { return my_classification; }
    void setClassification(string& classification) {my_classification = classification;}

    void UnigramTextClassifier::learn(istream& in);
    void UnigramTextClassifier::learn(char* in);
    
    void UnigramTextClassifier::dump(ostream& out);
    void UnigramTextClassifier::dump(char* out);

    void UnigramTextClassifier::read(istream& in);
    void UnigramTextClassifier::read(char* in);
  
    float UnigramTextClassifier::score(istream& in); 
    float UnigramTextClassifier::score(char* in) ;
     
    float UnigramTextClassifier::bits_required(unsigned char ch);
    float UnigramTextClassifier::bits_required(istream& in);
    float UnigramTextClassifier::bits_required(char* in);
  private:
    /* internal character->frequency map */
    frequency_map my_freqs;
    /* internal total number of characters in corpus */
    unsigned long my_corpus_total;
    /* internal total number of characters in text */
    unsigned long my_total;
    /* internal name of classifer */
    string my_classification;
    /* internal base-2 logarithm */
    float UnigramTextClassifier::lg (float n);
    /* internal information value function -lg(n) */
    float UnigramTextClassifier::info_value(float n);
    /* internal current time stream */
    string UnigramTextClassifier::ctime_string();
  };
}
using namespace std;

#endif /* UnigramTextClassifier_H_ */