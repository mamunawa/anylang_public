import unittest

def fun(x):
    return x + 1

class MyTest(unittest.TestCase):
    
    def test(self):
        self.assertEqual(fun(3), 4)
    
    def test_negative(self):
        self.assertEqual(fun(-1), 0)

if __name__ == '__main__':
    unittest.main()