// Ref: http://www.crankyotaku.com/2016/04/linux-programming-threading-basics.html
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

static int global = 0;
static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

static void *threadfunction(void *arg)
{
        int local, j, r;
        for(j = 0; j <=10; j++){
               r = pthread_mutex_lock(&lock);
               if( r !=0 ){
                      printf("Error getting lock\n");
                      exit(EXIT_FAILURE);
               }
  
               local = global;
               local++;
               global = local;

               r = pthread_mutex_unlock(&lock);
               if(r != 0){
                     printf("Error releasing lock\n");
                     exit(EXIT_FAILURE);
               }

        }
        return NULL;
}

int main(int argc, char *argv[])
{
        pthread_t thread1, thread2;
        int r;

        r = pthread_create(&thread1, NULL, threadfunction, NULL);
        if(r != 0){
               printf("Error creating thread\n");
               exit(EXIT_FAILURE);
        }
 
        r = pthread_create(&thread2, NULL, threadfunction, NULL);
        if(r != 0){
               printf("Error creating thread\n");
               exit(EXIT_FAILURE);
        }

        r = pthread_join(thread1, NULL);
        if(r != 0){
               printf("Error joining with thread\n");
               exit(EXIT_FAILURE);
        }
 
        r = pthread_join(thread2, NULL);
        if(r != 0){
               printf("Error joining with thread\n");
               exit(EXIT_FAILURE);
        }
        printf("global= %d\n", global);
        exit(EXIT_SUCCESS);

}




// The pthreads API also provides condition variables. See  pthread_cond_signal.

// compile with the option -pthread. 