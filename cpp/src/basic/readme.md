How to run
==========

% cat /ham/ham.0/* | ilearn Ham > ham.txt
% cat /spam/spam.0/* | ilearn Spam > spam.txt
% hamspam ham.txt spam.txt /ham/ham.1/*
Total: 500 Ham: 497 Spam: 3
% hamspam ham.txt spam.txt /spam/spam.1/*
Total: 500 Ham: 244 Spam: 256